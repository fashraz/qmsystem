<?php
session_start();
include('connect.php');

if(isset($_POST['login'])){

    $username=mysqli_real_escape_string($conn,$_POST['username']);
    $password=mysqli_real_escape_string($conn,$_POST['password']);

    if(empty($username)&&empty($password)){
    // $error= 'Fileds are Mandatory';
    $error="<script type='text/javascript'>alert('Fileds are Mandatory')</script>";
    }else{
        //Checking Login Detail
        $result=mysqli_query($conn,"SELECT*FROM employee WHERE BINARY username='$username' AND BINARY password='$password'");
        $row=mysqli_fetch_assoc($result);
        $count=mysqli_num_rows($result);
        if($count==1){
            $_SESSION['employee']=array(
            'username'=>$row['username'],
            'password'=>$row['password'],
            'employeeID'=>$row['employeeID'],
            'empName'=>$row['empName'],
            'level'=>$row['level']
            );
            $level=$_SESSION['employee']['level'];
            //Redirecting User Based on Role
            switch($level){
                case 'SAS':
                header('location: dashboard');
                break;
                case 'ADMIN':
                header('location: dashboardSA');
                break;
                case 'SALE ADVISOR':
                header('location: dashboardAdvisor');
                break;
                case 'MANAGER':
                header('location: dashboardManager');
                break;
            }
        }else{
        //$error='Your Password or Username is not Found';
        $error="<script type='text/javascript'>alert('Your Username or Password is not Found')</script>";
        }
    }
}
?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>QMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- DataTables -->
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
</head>
<body class="hold-transition login-page" style="background-color: #17a2b8">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-info">
    <div class="card-header text-center">
      <a href="" class="h1"><b>Queing Managment S</b>ystem</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="username" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user-tie"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-info btn-block" name="login">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <?php if(isset($error)){ echo $error; }?>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

    <!-- Default to the left -->
    <strong>Copyright &copy; 2021.</strong> 
        All rights reserved.
</body>
<style>
  :root{
    --gradient:linear-gradient(60deg,#97d4f7, #41b9f1, turquoise, aqua);
  }
  body{
  background-image: var(--gradient);
  background-size: 200%;
  animation: bg-animation 20s infinite alternate;
  }
  @keyframes bg-animation {
  0%   {background-position: left}

  100% {background-position: right;}
  }

</style>
</html>
