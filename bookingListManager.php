<?php
session_start();
include('connect.php');

//Checking User Logged or Not
if(empty($_SESSION['employee'])){
  header('location:index');
  }
  //Restrict user other than manager to access page
  if($_SESSION['employee']['level']=='ADMIN'){
  header('location:error.php');
  }
  if($_SESSION['employee']['level']=='SALE ADVISOR'){
  header('location:error.php');
  }
  if($_SESSION['employee']['level']=='SAS'){
  header('location:error.php');
  }

//display username
$empName = $_SESSION['employee']['empName'];
//display level
$level = $_SESSION['employee']['level'];

$result = mysqli_query($conn, "SELECT b.bookID, c.custID, c.custIC, cr.carID, e.employeeID, c.custName, e.empName, cr.model, cr.variant, cr.color, b.bookDate, b.bookStatus , b.bookingFee
                        FROM booking b
                        INNER JOIN customer c ON c.custID=b.custID
                        INNER JOIN employee e ON e.employeeID=b.employeeID
                        INNER JOIN car cr ON cr.carID=b.carID
                        ORDER BY b.bookStatus, b.bookDate, b.bookingFee DESC
");
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>QMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a href="dashboardManager.php" class="nav-link"></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboardManager.php" class="brand-link">
      <img src="dist/img/Perodua-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo strtoupper($level);?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/boy.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo strtoupper($empName);?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="dashboardManager.php" class="nav-link">
              <i class="fas fa-columns"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item menu-open">
            <a href="bookingListManager.php" class="nav-link active">
              <i class="fas fa-th-list"></i>
              <p>Booking
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="bookingListManager.php" class="nav-link active">
                  <i class="fas fa-circle"></i>
                  <p>Booking List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="approvalListManager.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Jump List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="cancelListManager.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Cancel List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="insuranceListManager.php" class="nav-link">
                <i class="far fa-circle"></i>
                <p>Insurance List</p>
                </a>
            </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="carListManager.php" class="nav-link">
              <i class="fas fa-car-side"></i>
              <p>Cars</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="userListManager.php" class="nav-link">
              <i class="fas fa-users"></i>
              <p>Users</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="announcementManager.php" class="nav-link">
              <i class="fas fa-bullhorn"></i>
              <p>Announcement</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="logout.php" onClick="return confirm('Are you sure you want to log out?')" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>Log Out</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><i class="fas fa-car-side"></i> BOOKING LIST</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
         <!--- masukkan content korang bawah row ni-->
         <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Booking Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 10px">No</th>
                            <th>Customer</th>
                            <th>IC</th>
                            <th>Sale Advisor</th>
                            <th>Model</th>
                            <th>Variant</th>
                            <th>Colour</th>
                            <th>Book Date</th>
                            <th>Booking Fee</th>
                            <th colspan=2> Status</th>
                            <th>Update</th>
                        </tr>
                    </thead>
                        <tbody>
                        <tr>
                            <?php
                                $number=1;
                                while ($user_data = mysqli_fetch_array($result)) {
                                $dates=$user_data['bookDate'];
                            ?>
                        <td><?php echo $number++; ?></td>
                        <td><?php echo $user_data['custName']; ?></td>
                        <td><?php echo $user_data['custIC']; ?></td>
		                <td><?php echo $user_data['empName']; ?></td>
                        <td><?php echo $user_data['model']; ?></td>
                        <td><?php echo $user_data['variant']; ?></td>
                        <td><?php echo $user_data['color']; ?></td>
                        <td><?php echo date("d/m/Y", strtotime($dates)); ?></td>
                        <td><?php echo $user_data['bookingFee']; ?></td>
                        <td>
                        <?php if ($user_data['bookStatus'] == 0){ echo "REGISTERED*";}
                              elseif ($user_data['bookStatus'] == 1){ echo "REGISTERED";}
                              elseif ($user_data['bookStatus'] == 2){ echo "ALLOCATED";}
                              elseif ($user_data['bookStatus'] == 3){ echo "DE-ALLOCATE";}
                              elseif ($user_data['bookStatus'] == 4){ echo "NEW ORDER";}
                              elseif ($user_data['bookStatus'] == 5){ echo "DELIVERED";}
                              else{ echo "CANCEL";}
                        ?>
                      </td>
                      <td>
                        <?php if ($user_data['bookStatus'] == 5){?>
                          <i class="fas fa-check-circle" style="color:green"></i>
                        <?php }
                              elseif ($user_data['bookStatus'] == 0 || $user_data['bookStatus'] == 1
                              || $user_data['bookStatus'] == 2 || $user_data['bookStatus'] == 3 || $user_data['bookStatus'] == 4){
                        ?>
                          <i class="fas fa-spinner" style="color:dimgray"></i>
                        <?php }
                              else{?>
                          <i class="fas fa-times-circle" style="color:red"></i>
                      </td>
                            <?php }
                            ?>
                        <td><a href="editBookingManager?id=<?php echo $user_data['bookID'];?>"><i class="fas fa-pencil-alt" style="color:dimgray"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Version 2.0
    </div>
    <!-- Default to the left -->
    
  </footer>
</div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>

<!-- Page specific script -->
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false ,  "ordering": false,
        "buttons": ["excel", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
  </script>

</body>
</html>
