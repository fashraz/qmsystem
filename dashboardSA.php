<?php
session_start();
include('connect.php');

//Checking User Logged or Not
if(empty($_SESSION['employee'])){
  header('location:index.php');
  }
  //Restrict user other than sales admin to access sa.php page
  if($_SESSION['employee']['level']=='SALE ADVISOR'){
  header('location: error.php');
  }
  if($_SESSION['employee']['level']=='SAS'){
  header('location: error.php');
  }
  if($_SESSION['employee']['level']=='MANAGER'){
  header('location: error.php');
  }
  
//display username
$empName = $_SESSION['employee']['empName'];
//display level
$level = $_SESSION['employee']['level'];

//get current year
$bookDate = date("Y");

//get current month
$MonthResult = mysqli_query($conn, "SELECT MONTHNAME(CURDATE()) as Month");
while($user_data = mysqli_fetch_array($MonthResult))
                              {$monthName = $user_data['Month'];}

//sql to get booking data by month per year
$month = mysqli_query($conn, "SELECT MONTHNAME(bookDate) AS months, COUNT(bookID) AS numBook FROM booking WHERE YEAR(bookDate) =$bookDate  GROUP BY MONTHNAME(bookDate) Order By bookDate");

//sql monthly booking
$number = 1;
while ($user_data = mysqli_fetch_array($month)) {
  $m[] = $user_data['months'];
  $b[] = $user_data['numBook'];
}

//sql to get variant
$variant = mysqli_query($conn, "SELECT Variant As Variant, COUNT(variant) As Num FROM car c INNER JOIN booking b ON b.carID=c.carID  WHERE MONTHNAME(bookDate)=MONTHNAME(CURDATE()) GROUP BY Variant");

//sql variant in a year
while ($user_data = mysqli_fetch_array($variant)) {
  $v[] = $user_data['Variant'];
  $n[] = $user_data['Num'];
}

//sql today booking
$TodayBook = mysqli_query($conn, "SELECT count(bookID) FROM booking WHERE bookDate=CURDATE()");
$todaks = mysqli_fetch_array($TodayBook);
$Totaltoday = $todaks[0];

//sql monthly booking
$result2 = mysqli_query($conn, "SELECT count(bookID) from booking where MONTH(bookDate)=MONTH(CURDATE())");
$monthlyBook = mysqli_fetch_array($result2);
$totalMonth = $monthlyBook[0];

//performance sql
$result = mysqli_query($conn, "SELECT employee.empName AS  SAName,COUNT(booking.employeeID) AS Sales
                                FROM booking 
                                INNER JOIN employee ON booking.employeeID = employee.employeeID
                                WHERE booking.bookStatus != 5 AND MONTH(bookDate)=MONTH(CURDATE())
                                GROUP BY booking.employeeID
                                ORDER BY COUNT(booking.employeeID) DESC");
//announcement list
$result3 =  mysqli_query($conn, "SELECT * FROM announcement ORDER BY ancmtID DESC ");

//announcement list
$queryLatest =  mysqli_query($conn, "SELECT MAX(ancmtID) FROM announcement ");
$resultMax = mysqli_fetch_array($queryLatest);
$max = $resultMax[0];
?>


<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>QMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
          </a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a href="dashboardSA.php" class="nav-link"></a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-primary elevation-4">
      <!-- Brand Logo -->
      <a href="dashboardSA.php" class="brand-link">
        <img src="dist/img/Perodua-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?php echo strtoupper($level);?></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="dist/img/boy.png" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block"><?php echo strtoupper($empName);?></a>
          </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->

            <li class="nav-item">
              <a href="#" class="nav-link active">
                <i class="fas fa-columns"></i>
                <p>
                  Dashboard
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
            </li>

            <li class="nav-item menu">
            <a href="bookingLisSA.php" class="nav-link">
              <i class="fas fa-th-list"></i>
              <p>Booking<i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="bookingListSA.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Booking List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="approvalListSA.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Jump List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="cancelListSA.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Cancel List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="insuranceListSA.php" class="nav-link">
                <i class="far fa-circle"></i>
                <p>Insurance List</p>
                </a>
              </li>
            </ul>
          </li>

            <!-- <li class="nav-item">
              <a href="carList.php" class="nav-link">
                <i class="fas fa-car-side"></i>
                <p>
                  Cars
                </p>
              </a>
            </li> -->

            <!-- <li class="nav-item">
              <a href="userListAdmin.php" class="nav-link">
                <i class="fas fa-users"></i>
                <p>
                  Users
                </p>
              </a>
            </li> -->
            <li class="nav-item">
              <a href="logout.php" onClick="return confirm('Are you sure you want to log out?')" class="nav-link">
                <i class="fas fa-sign-out-alt"></i>
                <p>Log Out</p>
              </a>
            </li>

          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0"><i class="fas fa-columns"></i> Dashboard</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small card -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3><?php echo $Totaltoday; ?></h3>
                  <p>Today's Booking</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="todayBookingSA.php" target="_blank" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-6">
              <!-- small card -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3><?php echo $totalMonth; ?></h3>
                  <p>Monthly Booking</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="monthlyBookingSA.php" target="_blank" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->

            <div class="col-md-6">
              <div class="card card-warning">
                <div class="card-header">

                  <h3 class="card-title"><i class="fas fa-comments"></i> Announcement</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <!-- annoucement -->
                  <ul>
                    <?php
                    while ($user_data = mysqli_fetch_array($result3)) {
                      $id = $user_data['employeeID'];
                      $result4 = mysqli_query($conn, "SELECT * FROM employee where employeeID=$id");
                      while ($user_data2 = mysqli_fetch_array($result4)) {
                    ?>
                        <?php if ($user_data['ancmtID'] == $max) { ?>
                          <li style="list-style: square;"><?php echo $user_data['text']; ?> - <?php echo $user_data2['empName']; ?> <span class="right badge badge-danger">New</span></li> <?php } ?>
                        <?php if ($user_data['ancmtID'] != $max) { ?>
                          <li style="list-style: square;"><?php echo $user_data['text']; ?> - <?php echo $user_data2['empName']; ?></li> <?php } ?>
                      <?php } ?>
                    <?php } ?>
                    <ul>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          <div class="row">
            <div class="col-md-6">
              <div class="card card-maroon">
                <div class="card-header">
                  <h3 class="card-title">Monthly Booking - <?php echo $bookDate ?></h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
              <div class="card card-purple">
                <div class="card-header">
                  <h3 class="card-title">Performance - <?php echo $monthName?></h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="table">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th style="width: 5px">Rank</th>
                          <th>Sale Advisor</th>
                          <th>Total Booking</th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php
                          $number3 = 1;
                          while ($user_data = mysqli_fetch_array($result)) {  ?>
                            <td><?php echo $number3++; ?></td>
                            <td><?php echo $user_data['SAName']; ?></td>
                            <td><?php echo $user_data['Sales']; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          <div class="row">
            <div class="col-md-6">
              <div class="card card-lightblue">
                <div class="card-header">
                  <h3 class="card-title">Variant - <?php echo $monthName?></h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="varChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Version 2.0
      </div>
      <!-- Default to the left -->
      
    </footer>
  </div>

  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>

  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>


  <!-- ChartJS -->
  <script src="../../plugins/chart.js/Chart.min.js"></script>


  <!-- barchart -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>


 <!-- script bar monthly -->
 <script type="text/javascript">
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#292b2c';

    // Bar Chart Example
    var ctx = document.getElementById("barChart");
    var myLineChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: <?php echo json_encode($m); ?>,
        datasets: [{
          label: "Total",
          backgroundColor: "#d81b60",

          borderColor: "rgba(2,117,216,1)",
          data: <?php echo json_encode($b); ?>,
        }],
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'Total'
            },
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            ticks: {
              min: 0,
              precision: 0
            },
            gridLines: {
              display: true
            }
          }],
        },
        legend: {
          display: false
        }
      }
    });

    //script bar variant 
    // Bar Chart Example
    var ctx2 = document.getElementById("varChart");
    var myLineChart2 = new Chart(ctx2, {
      type: 'bar',
      data: {
        labels: <?php echo json_encode($v); ?>,
        datasets: [{
          label: "Total",
          backgroundColor: "#3c8dbc",
          borderColor: "rgba(30, 130, 76, 1)",
          data: <?php echo json_encode($n); ?>,
        }],
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'Total'
            },
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            ticks: {
              min: 0,
              precision: 0
            },
            gridLines: {
              display: true
            }
          }],
        },
        legend: {
          display: false
        }
      }
    });
  </script>

  <!-- DataTables  & Plugins -->
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
  <script src="plugins/jszip/jszip.min.js"></script>
  <script src="plugins/pdfmake/pdfmake.min.js"></script>
  <script src="plugins/pdfmake/vfs_fonts.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

  <!-- Page specific script -->
  <script>
    $(function() {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "ordering": false,
        "pageLength":5,
        "searching": false
      });

    });
  </script>

</body>

</html>