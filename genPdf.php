<?php
session_start();
$bookID = $_GET['id'];
include('connect.php');

//error_reporting(E_ALL); 
//ini_set('display_errors', 1);

$errors = array();
$results = mysqli_query($conn, "SELECT c.custName, c.custIC, c.maritalStatus, c.occupation, c.phoneNo, c.email, c.address, 
                                b.paymentType, b.bookDate, b.bookingFee, b.vsoNo, b.receiptNo, b.bookStatus, b.loanAmount, 
                                b.loanPeriod, b.branchBank, b.reason, b.reasonAdmin, b.returnDate, b.bankDate, b.collectionDate, b.disbursedDate, 
                                e.empName 
                                FROM booking b 
                                INNER JOIN customer c ON b.custID = c.custID 
                                INNER JOIN employee e ON e.employeeID = b.employeeID WHERE 
                                b.bookID='$bookID' ");
    while($res = mysqli_fetch_array($results))
    {
        //$custID = $res['custID']; 
        $vsoNo = $res['vsoNo'];
        $custName = $res['custName'];
        $custIC = $res['custIC'];
        $maritalStatus = $res['maritalStatus'];
        $occupation = $res['occupation'];
        $phoneNo = $res['phoneNo'];
        $email = $res['email'];
        $address = $res['address'];
        $bookDate = $res['bookDate'];
        $bookingFee = $res['bookingFee'];
        $receiptNo = $res['receiptNo'];
        $paymentType = $res['paymentType'];
        $returnDate = $res['returnDate'];
        $bankDate = $res['bankDate'];
        $collectionDate = $res['collectionDate'];
        $disbursedDate = $res['disbursedDate'];
        $bookStatus = $res['bookStatus'];
        $reason = $res['reason'];
        $reasonAdmin = $res['reasonAdmin'];

        $empName2 = $res['empName'];
        $branchBank = $res['branchBank'];
        $amountloan = $res['loanAmount'];
        $loanPeriod = $res['loanPeriod'];

    }
$results1 = mysqli_query($conn, "SELECT c.carID, c.model, c.variant, c.color FROM car c
                                INNER JOIN booking b ON c.carID=b.carID
                                WHERE b.bookID='$bookID'");
    while($res1 = mysqli_fetch_array($results1))
    {	
        $carID = $res1['carID'];
        $model = $res1['model'];
        $variant = $res1['variant'];
        $color = $res1['color'];
    }
$results2 = mysqli_query($conn, "SELECT bank, registerDate, louNo, louDate, plateNo, roadTax, insuranceCompany, allocateDate,
                                chasisNo, engineNo, yearMake FROM carinfo 
                                INNER JOIN employee e ON employeeID=employeeID
                                WHERE bookID='$bookID'");
    while($res2 = mysqli_fetch_array($results2))
    {
        //$custID = $res['custID'];
        $bank = $res2['bank'];
        $engineNo = $res2['engineNo'];
        $louNo = $res2['louNo'];
        $yearMake = $res2['yearMake'];
        $louDate = $res2['louDate'];
        $chasisNo = $res2['chasisNo'];
        $allocateDate = $res2['allocateDate'];
        $registerDate = $res2['registerDate'];
        $plateNo = $res2['plateNo'];
        $roadTax = $res2['roadTax'];
        $insuranceCompany = $res2['insuranceCompany'];

    }

 $results3 = mysqli_query($conn, "SELECT insuranceID, inCompany, inPolicy, inDateB FROM insurance
                                    WHERE bookID= '$bookID'");

    while($res3 = mysqli_fetch_array($results3))
    {
        $inCompany = $res3['inCompany'];
        $inPolicy = $res3['inPolicy'];
        $inDateB = $res3['inDateB'];

    }
$result = mysqli_query($conn, "SELECT * FROM car");



require('fpdf/fpdf.php');

$pdf = new FPDF();
$pdf->SetTitle($bookID.'-'.$vsoNo);
$pdf->AddPage();
$pdf->AddFont('calibri','','calibri-regular.php');
$pdf->AddFont('calibri','BI','calibri-bold-italic.php');
$pdf->AddFont('calibri','B','calibri-bold.php');
$pdf->AddFont('calibri','I','calibri-italic.php');

$text="KERETA AUTOMOBIL SDN. BHD.";
$text1=" (567975-X)";
$text2= "alamat kedai kereta";



$size=13;
$font='calibri';
  
	//Background Image
    $img=$pdf->Image('dist/img/Perodua-logo3.png',5,14,32,20);
    $pdf->SetXY(42,15);
    $pdf->SetFont($font,'BI',16);
    $pdf->MultiCell(130,5,$text,0,'L',0);   
    $pdf->SetXY(123,15);
    $pdf->SetFont($font,'I',10);
    $pdf->MultiCell(130,5,$text1,0,'L',0);
    $pdf->SetX(42);
    $pdf->SetFont($font,'I',10);
    $pdf->MultiCell(135,4,$text2,0,'L',0);


	//table
    $pdf->Ln(10);
    $pdf->SetXY(160,18);
    $pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'VSO No : ',0,0,'R',0);
	$pdf->SetFont('Arial','',$size);
	$pdf->Cell(70,10,$vsoNo,0,1,'L',0);
    $pdf->SetXY(160,24);
    $pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Book Date : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$bookDate,0,1,'L',0);
    $pdf->SetXY(160,30);
    $pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'LOU Date : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$louDate,0,1,'L',0);
    
    $pdf->Ln(10);
    $pdf->SetX(20);
	$pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Customer Name : ',0,0,'R',0);
    $pdf->SetFont($font,'',$size);
    $pdf->MultiCell(130,10,$custName,0,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Identification No. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$custIC,0,1,'L',0);
    $pdf->SetX(20);
	$pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Marital Status : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$maritalStatus,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Occupation : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$occupation,0,1,'L',0);
    $pdf->SetX(20);
	$pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Phone No : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$phoneNo,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Email : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$email,0,1,'L',0);
    $pdf->SetX(20);
	$pdf->SetFont($font,'B',$size);
	$pdf->Cell(22,10,'Address : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->MultiCell(130,10,$address,0,'L',0);

    $pdf->SetX(20);
	$pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Model : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$model,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Colour : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$color,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Engine No. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$engineNo,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Chassis No. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$chasisNo,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Insurance Co. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$inCompany,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Policy No. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$inPolicy,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Finance Co. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$bank,0,0,'L',0);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Branch : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$branchBank,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Amount Loaned : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,'RM '.$amountloan,0,0,'L',0);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Period : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10, $loanPeriod.'    MONTHS',0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Salasman. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->Cell(70,10,$empName2,0,1,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,'Remarks. : ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->MultiCell(130,10,'Admin : '.$reason,0,'L',0);
    $pdf->SetX(20);
    $pdf->SetFont($font,'B',$size);
    $pdf->Cell(22,10,' ',0,0,'R',0);
	$pdf->SetFont($font,'',$size);
	$pdf->MultiCell(130,10,'SAS : '.$reasonAdmin,0,'L',0);
    $pdf->Output($bookID.'-'.$vsoNo,'I');

?>

