<?php
session_start();
include('connect.php');

//Checking User Logged or Not
if(empty($_SESSION['employee'])){
  header('location: index.php');
  }
  //Restrict user other than advisor to access page
  if($_SESSION['employee']['level']=='SAS'){
    header('location: error');
    }
  if($_SESSION['employee']['level']=='MANAGER'){
    header('location:error');
  }
  if($_SESSION['employee']['level']=='ADMIN'){
  header('location: error');
  }

//id username
$employeeID = $_SESSION['employee']['employeeID'];
//display username
$empName = $_SESSION['employee']['empName'];
//display level
$level = $_SESSION['employee']['level'];
$errors = array();
if (isset($_POST['addBooking'])) {
    //receive all input values from the form
    $custName = mysqli_real_escape_string($conn, strtoupper($_POST['custName']));
    $custIC = mysqli_real_escape_string($conn, strtoupper($_POST['custIC']));
    $maritalStatus = mysqli_real_escape_string($conn, $_POST['maritalStatus']);
    $occupation = mysqli_real_escape_string($conn, strtoupper($_POST['occupation']));
    $email = mysqli_real_escape_string($conn, strtoupper($_POST['email']));
    $phoneNo = mysqli_real_escape_string($conn, strtoupper($_POST['phoneNo']));
    $address = mysqli_real_escape_string($conn, strtoupper($_POST['address']));
    $carID = mysqli_real_escape_string($conn, strtoupper($_POST['carID']));
    $bookingFee = mysqli_real_escape_string($conn, $_POST['bookingFee']);


    //form validation: ensure that the form is correctly filled
    // if(empty($custName)){array_push($errors, "Customer Name is required");}
    // if(empty($custIC)){array_push($errors, "Customer Identification is required");}
    // if(empty($maritalStatus)){array_push($errors, "Marital status is required");}
    // if(empty($occupation)){array_push($errors, "Occupation is required");}
    // if(empty($email)){array_push($errors, "Email is required");}
    // if(empty($phoneNo)){array_push($errors, "Phone number is required");}
    // if(empty($address)){array_push($errors, "Address is required");}
    // if(empty($carID)){array_push($errors, "Car is required");}
    // if(empty($bookingFee)){array_push($errors, "Booking fee is required");}
    // if(empty($receiptNo)){array_push($errors, "Receipt number is required");}

    //user if there are no errors in the form
    if(count($errors) == 0){
        $query = "INSERT INTO customer (custName, custIC, maritalStatus, occupation, email, phoneNo, address)
        VALUES('$custName','$custIC','$maritalStatus','$occupation','$email','$phoneNo','$address')";
        mysqli_query($conn, $query);
        $custID = mysqli_insert_id($conn);

        $query2 = "INSERT INTO booking (employeeID,custID,carID,bookDate,bookingFee,bookStatus)
        VALUES ('$employeeID','$custID','$carID',NOW(),'$bookingFee',4)";
        mysqli_query($conn, $query2);
        $bookID = mysqli_insert_id($conn);

        $query3 = "INSERT INTO carinfo (bookID,carID)
        VALUES ('$bookID','$carID')";
        mysqli_query($conn, $query3);

        $query4 = "INSERT INTO insurance (bookID,custID)
        VALUES ('$bookID','$custID')";
        mysqli_query($conn, $query4);


        echo "<script type='text/javascript'>alert('Booking added successfully.')</script>";
    }
    else
    echo "<script type='text/javascript'>alert('failed!')</script>";
}
?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>QMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- DataTables -->
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a href="dashboardAdvisor.php" class="nav-link"></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboardAdvisor.php" class="brand-link">
      <img src="dist/img/Perodua-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo strtoupper($level);?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/boy.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo strtoupper($empName);?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="dashboardAdvisor.php" class="nav-link">
              <i class="fas fa-columns"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item menu-open">
            <a href="bookingListAdvisor.php" class="nav-link active">
              <i class="fas fa-th-list"></i>
              <p>Booking<i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="bookingListAdvisor.php" class="nav-link active">
                  <i class="fas fa-circle"></i>
                  <p>Booking List</p>
                </a>
              </li>
                <li class="nav-item">
                  <a href="approvalListAdvisor.php" class="nav-link">
                    <i class="far fa-circle"></i>
                    <p>Jump List</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="cancelListAdvisor.php" class="nav-link">
                    <i class="far fa-circle"></i>
                    <p>Cancel List</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="insuranceListAdvisor.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Insurance List</p>
                  </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="logout.php" onClick="return confirm('Are you sure you want to log out?')" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>Log Out</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Add Booking</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
         <!--- masukkan content korang bawah row ni-->

          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Customer Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" method="post">
                <div class="card-body">

                  <div class="form-group">
                    <label for="text-field">Customer Name</label>
                    <input class="form-control" name="custName" type="text" placeholder="Enter Name" required/>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Identification Number</label>
                    <input class="form-control" name="custIC" type="text" placeholder="Enter Identification Number" required/>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Marital Status</label>
                      <select  class="form-control" name="maritalStatus" placeholder="Choose Marital Status" required>
                        <option hidden value="">Please Choose:</option>
                        <option value="SINGLE">SINGLE</option>
                        <option value="MARRIED">MARRIED</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Occupation</label>
                    <input class="form-control" name="occupation" type="text" placeholder="Enter Occupation" required/>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Phone Number</label>
                    <input class="form-control" name="phoneNo" type="text" placeholder="Enter Phone Number" required/>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Email Address</label>
                    <input class="form-control" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter Email Address" required/>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Address</label>
                    <input class="form-control" name="address" type="text" placeholder="Enter Address" required/>
                  </div>
                  <div class="form-group">
                    <label>Booking Fee</label>
                        <select class="form-control select2" style="width: 100%;" name="bookingFee" required>
                            <option value="">Please Select:</option>
                            <option value="100">100</option>
                            <option value="300">300</option>
                        </select>
                  </div>

                  <div class="form-group">
                    <label for="text-field">Model</label>
                        <select  class="form-control" name="f" id="model-list" class="demoInputBox"  onChange="getColor(this.value);" required>
                        <option value="">Select Model</option>
                        <?php
                          $sql = "SELECT * FROM car";
                          $res = mysqli_query($conn, $sql);
                          while ($row = mysqli_fetch_assoc($res))
                          {
                            if ($bul[$row['model']] != true && $row['model'] != 'Model')
                            {
                        ?>
                              <option value="<?php echo $row['model']; ?>"><?php echo $row['model']; ?></option>
                              <?php
                              $bul[$row['model']] = true;
                            }
                          }
                                  ?>
                        </select>
                  </div>
                  <div class="form-group">
                    <label for="text-field">Color</label>
                    <select  class="form-control" name="carID" id="color-list" class="demoInputBox" required>
                      <option value="carID">Select Color</option>
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <a class="btn btn-primary mr-3" href="bookingListAdvisor.php">Back</a>
                      <button class="btn btn-primary" type="submit" name="addBooking">Submit</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Version 2.0
    </div>
    <!-- Default to the left -->
  </footer>
</div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>
<!-- data table script cdn *template lain -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="dist/js/databables-demo.js"></script>

</body>
</html>
<script>
function getColor(val) {
$.ajax({
  type: "POST",
  url: "get_color2.php",
  data:'color_id='+val,
  success: function(data){
  $("#color-list").html(data);
  }
  });
}
function selectModel(val) {
$("#search-b-ox").val(val);
$("#suggesstion-box").hide();
}
</script>
<script>
$( "select[name='model']" ).change(function () {
  var carID = $(this).val();
  if(carID) {
    $.ajax({
      url: "ajaxpro.php",
      dataType: 'Json',
      data: {'id':carID},
      success: function(data) {
        $('select[name="variant"]').empty();
        $.each(data, function(key, value) {
          $('select[name="variant"]').append('<option value="'+ key +'">'+ value +'</option>');
        });
      }
    });
  }else{
    $('select[name="variant"]').empty();
  }
});
</script>
