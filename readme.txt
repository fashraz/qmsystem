This is simple booking management system using native php, javascript, bootstrap, jquery.

--require software--
1. xampp
2. vsc

--file preparation--
3. clone/download file
4. locate file in xampp/htdocs/

--database preparation--
5. open xampp, click "start" for  mysql
6. import database (qms.sql) and name it as "qms"

--to run the system--
7. open xampp, click "start" for apache
8. to start, open "http://localhost/<your_folder_name>/index.php"
