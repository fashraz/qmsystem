<?php
session_start();
include('connect.php');

//Checking User Logged or Not
if(empty($_SESSION['employee'])){
  header('location:index');
 }
 //Restrict users other than supperintendent to access page
 if($_SESSION['employee']['level']=='MANAGER'){
  header('location:error.php');
 }
 if($_SESSION['employee']['level']=='ADMIN'){
  header('location:error.php');
 }
 if($_SESSION['employee']['level']=='SALE ADVISOR'){
  header('location:error.php');
 }

//display empName
$empName2 = $_SESSION['employee']['empName'];
//display level
$level2 = $_SESSION['employee']['level'];
$errors = array();

if (isset($_POST['addStaff'])) {

    // receive all input values from the form
    $staffID = mysqli_real_escape_string($conn, $_POST['staffID']);
    $empName = mysqli_real_escape_string($conn, strtoupper($_POST['empName']));
    $empPosition = mysqli_real_escape_string($conn, strtoupper($_POST['empPosition']));
    $empPhone = mysqli_real_escape_string($conn, $_POST['empPhone']);
    $username = mysqli_real_escape_string($conn, $_POST['username']); 
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    $level = mysqli_real_escape_string($conn, $_POST['level']); 
  
    // // form validation: ensure that the form is correctly filled
    // if (empty($staffID)) { array_push($errors, "Identity Card Number is required"); }
    // if (empty($empName)) { array_push($errors, "Full Name is required"); }
    // if (empty($empPosition)) { array_push($errors, "Position is required"); }
    // if (empty($empPhone)) { array_push($errors, "Phone Number is required"); }
    // if (empty($username)) { array_push($errors, "Username is required"); }
    // if (empty($password)) { array_push($errors, "Password is required"); }
    // //if (empty($level)) { array_push($errors, "Level is required"); }  
  
    // register user if there are no errors in the form
    if (count($errors) == 0) {
      
        $result = "INSERT INTO employee (staffID,empName,empPosition,empPhone,username,password,level) 
            VALUES ('$staffID','$empName','$empPosition','$empPhone','$username','$password','$level')";
            
        mysqli_query($conn, $result);
      
      echo "<script type='text/javascript'>alert('User added successfully.')</script>";
      
      
    }
    else
        echo "<script type='text/javascript'>alert('failed!')</script>";
  }
?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>QMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- DataTables -->
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-footer-fixed">

<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item">
        <a href="dashboardAdmin.php" class="nav-link"></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboardAdmin.php" class="brand-link">
      <img src="dist/img/Perodua-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo strtoupper($level2);?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/boy.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo strtoupper($empName2);?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="dashboardAdmin.php" class="nav-link">
              <i class="fas fa-columns"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item menu">
              <a href="bookingListAdmin.php" class="nav-link">
              <i class="fas fa-th-list"></i>
              <p>Booking</p>
              </a>
              <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="bookingListAdmin.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Booking List</p>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="approvalListAdmin.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Jump List</p>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="cancelListAdmin.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Cancel List</p>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="insuranceListAdmin.php" class="nav-link ">
                  <i class="far fa-circle"></i>
                  <p>Insurance List</p>
                  </a>
              </li>
              </ul>
          </li>
          <li class="nav-item">
            <a href="carListAdmin.php" class="nav-link">
              <i class="fas fa-car-side"></i>
              <p>Cars</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="userListAdmin.php" class="nav-link active">
              <i class="fas fa-users"></i>
              <p>Users<i class="right fas fa-angle-left"></i></p>
            </a>
          </li>
          <li class="nav-item">
            <a href="announcement.php" class="nav-link">
              <i class="fas fa-bullhorn"></i>
              <p>Announcement</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="logout.php" onClick="return confirm('Are you sure you want to log out?')" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p>Log Out</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">ADD USER</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
         <!--- masukkan content korang bawah row ni-->

          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Employee Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="empName" class="form-control" id="" placeholder="Enter Full Name" required>
                  </div>
                  <div class="form-group">
                    <label for="">Staff Number</label>
                    <input type="text" name="staffID" class="form-control" id="" placeholder="Enter Staff Number" required>
                  </div>
                  <div class="form-group">
                    <label for="">Phone Number</label>
                    <input type="text" name="empPhone" class="form-control" id="" placeholder="Enter Phone Number" required>
                  </div>
                  <div class="form-group">
                    <label for="">Position</label>
                    <input type="text" name="empPosition" class="form-control" id="" placeholder="Enter Position" required>
                  </div>
                  <div class="form-group">
                    <label>Level</label>
                        <select class="form-control select2" style="width: 100%;" name="level" required>
                          <option value="">Please Select:</option>
                          <option value="SAS">SAS</option>
                          <option value="MANAGER">MANAGER</option>  
                          <option value="ADMIN">ADMIN</option>
                          <option value="SALE ADVISOR">SALE ADVISOR</option>                   
                        </select>
                  </div>
                  <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" name="username" class="form-control" id="" placeholder="Enter Username" required>
                  </div>
                  <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" class="form-control" id="" placeholder="Enter Password" required>
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <a class="btn btn-primary mr-3" href="userListAdmin.php">Back</a>
                      <button class="btn btn-primary" type="submit" name="addStaff">Submit</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Version 2.0
    </div>
    <!-- Default to the left -->
    
  </footer>
</div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>
<!-- data table script cdn *template lain -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="dist/js/databables-demo.js"></script>

</body>
</html>
