<?php
session_start();
$bookID = $_GET['id'];
include('connect.php');

//Checking User Logged or Not
if(empty($_SESSION['employee'])){
    header('location:index');
    }
    //Restrict user other than manager to access page
    if($_SESSION['employee']['level']=='ADMIN'){
    header('location:error.php');
    }
    if($_SESSION['employee']['level']=='SALE ADVISOR'){
    header('location:error.php');
    }
    if($_SESSION['employee']['level']=='SAS'){
    header('location:error.php');
    }

    //display username
    $empName = $_SESSION['employee']['empName'];
    //display level
    $level = $_SESSION['employee']['level'];


$errors = array();
if (isset($_POST['updateBooking'])) {
    //receive all input values from the form

    $insuranceCompanyN = mysqli_real_escape_string($conn, strtoupper($_POST['insuranceCompanyN']));
    $policyN =  mysqli_real_escape_string($conn, strtoupper($_POST['policyN']));
    $periodfromN = mysqli_real_escape_string($conn, strtoupper($_POST['periodfromN']));
    $periodtoN = mysqli_real_escape_string($conn, strtoupper($_POST['periodtoN']));



    //user if there are no errors in the form
    if(count($errors) == 0){


        $query3 = "UPDATE insurance SET  inCompany='$insuranceCompanyN', inPolicy='$policyN', inDateA='$periodfromN',
                    inDateB='$periodtoN' WHERE bookID='$bookID'";
        mysqli_query($conn, $query3);



        echo "<script type='text/javascript'>alert('Update booking successfully.')</script>";
    }
    else
    echo "<script type='text/javascript'>alert('failed!')</script>";
}
if (isset($_POST['insertDown'])) {

    $downAmountN = mysqli_real_escape_string($conn, strtoupper($_POST['downAmountN']));
    $downTypeN = mysqli_real_escape_string($conn, strtoupper($_POST['downTypeN']));
    $downReceiptN = mysqli_real_escape_string($conn, strtoupper($_POST['downReceiptN']));
    //user if there are no errors in the form
    if(count($errors) == 0){
        $query4 = "INSERT INTO downpayment (downAmount, downMethod, downReceipt, bookID)  VALUE ('$downAmountN','$downTypeN','$downReceiptN','$bookID')";
        mysqli_query($conn, $query4);
    }

}
if (isset($_POST['deleteDown'])) {
    //user if there are no errors in the form
    if(count($errors) == 0){
        $query5 = "DELETE FROM downpayment WHERE bookID='$bookID'";
        mysqli_query($conn, $query5);
    }

}

    $results = mysqli_query($conn, "SELECT c.custName, c.custIC, c.maritalStatus, c.occupation, c.phoneNo, c.email, c.address,
                                    b.paymentType, b.loanAmount,b.loanPeriod, b.branchBank, b.bookDate, b.bookingFee, b.vsoNo,
                                    b.receiptNo, b.bookStatus, b.reason, b.reasonAdmin, b.returnDate, b.bankDate, b.collectionDate,
                                    b.disbursedDate, e.empName
                                    FROM booking b
                                    INNER JOIN customer c ON b.custID = c.custID
                                    INNER JOIN employee e ON b.employeeID = e.employeeID
                                    WHERE b.bookID='$bookID'");
    while($res = mysqli_fetch_array($results))
    {
        //$custID = $res['custID'];
        $vsoNo = $res['vsoNo'];
        $custName = $res['custName'];
        $custIC = $res['custIC'];
        $maritalStatus = $res['maritalStatus'];
        $occupation = $res['occupation'];
        $phoneNo = $res['phoneNo'];
        $email = $res['email'];
        $address = $res['address'];
        $bookDate = $res['bookDate'];
        $bookingFee = $res['bookingFee'];
        $receiptNo = $res['receiptNo'];
        $paymentType = $res['paymentType'];
        $loanAmount =$res['loanAmount'];
        $branch =$res['branchBank'];
        $loanPeriod =$res['loanPeriod'];
        $returnDate = $res['returnDate'];
        $bankDate = $res['bankDate'];
        $collectionDate = $res['collectionDate'];
        $disbursedDate = $res['disbursedDate'];
        $bookStatus = $res['bookStatus'];
        $reason = $res['reason'];
        $reasonAdmin = $res['reasonAdmin'];
        $sa = $res['empName'];
    }
    $results1 = mysqli_query($conn, "SELECT c.carID, c.model, c.variant, c.color FROM car c
                                INNER JOIN booking b ON c.carID=b.carID
                                WHERE b.bookID='$bookID'");
    while($res1 = mysqli_fetch_array($results1))
    {
        $carID = $res1['carID'];
        $model = $res1['model'];
        $variant = $res1['variant'];
        $color = $res1['color'];
    }
    $results2 = mysqli_query($conn, "SELECT bank, registerDate, louNo, louDate, plateNo, roadTax, insuranceCompany, allocateDate,
                                chasisNo, engineNo, yearMake FROM carinfo WHERE bookID='$bookID'");
    while($res2 = mysqli_fetch_array($results2))
    {
        //$custID = $res['custID'];
        $bank = $res2['bank'];
        $engineNo = $res2['engineNo'];
        $louNo = $res2['louNo'];
        $yearMake = $res2['yearMake'];
        $louDate = $res2['louDate'];
        $chasisNo = $res2['chasisNo'];
        $allocateDate = $res2['allocateDate'];
        $registerDate = $res2['registerDate'];
        $plateNo = $res2['plateNo'];
        $roadTax = $res2['roadTax'];
        $insuranceCompany = $res2['insuranceCompany'];
    }
    $result3 = mysqli_query($conn, "SELECT * FROM car");

    $results4 = mysqli_query($conn, "SELECT inCompany, inPolicy, inDateA, inDateB FROM insurance WHERE bookID='$bookID'");
    while($res4 = mysqli_fetch_array($results4))
    {
        $insuranceCompany = $res4['inCompany'];
        $policyNo = $res4['inPolicy'];
        $periodfrom = $res4['inDateA'];
        $periodto = $res4['inDateB'];
    }
    $results5 = mysqli_query($conn, "SELECT downAmount, downMethod, downReceipt, downpayID FROM downpayment WHERE bookID='$bookID'");

?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>QMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="dashboardManager.php" class="nav-link"></a>
            </li>
        </ul>
    </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
      <img src="dist/img/Perodua-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo strtoupper($level);?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/boy.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo strtoupper($empName);?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="dashboardManager.php" class="nav-link">
              <i class="fas fa-columns"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item menu-open">
            <a href="bookingListManager.php" class="nav-link active">
              <i class="fas fa-th-list"></i>
              <p>Booking<i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="bookingListManager.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Booking List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="approvalListManager.php" class="nav-link ">
                  <i class="far fa-circle"></i>
                  <p>Jump List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="cancelListManager.php" class="nav-link">
                  <i class="far fa-circle"></i>
                  <p>Cancel List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="insuranceListManager.php" class="nav-link active">
                <i class="fas fa-circle"></i>
                <p>Insurance List</p>
                </a>
            </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="carListManager.php" class="nav-link">
              <i class="fas fa-car-side"></i>
              <p>Cars</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="userListManager.php" class="nav-link">
              <i class="fas fa-users"></i>
              <p>Users</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="announcementManager.php" class="nav-link">
              <i class="fas fa-bullhorn"></i>
              <p>Announcement</p>
            </a>
          </li>
          <li class="nav-item">
              <a href="logout.php"onClick="return confirm('Are you sure you want to log out?')" class="nav-link">
                <i class="fas fa-sign-out-alt"></i>
                <p>Log Out</p>
              </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Insurance</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">

      <div class="container-fluid">
        <form method="post">
          <div class="row">
            <!--- masukkan content korang bawah row ni-->

             <!-- left column -->
             <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Customer Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="text-field">VSO Number</label>
                            <input type="text" class="form-control" id="vsoID" name="vsoNo" value="<?php echo $vsoNo;?>" placeholder="Enter VSO Number"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Booking Receipt Number</label>
                            <input type="text" class="form-control"  id="receiptID" name="receiptNo" value="<?php echo $receiptNo;?>"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Customer Name</label>
                            <input type="text" class="form-control"  id="custID" name="custName" value="<?php echo $custName;?>"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Identification Number</label>
                            <input type="text" class="form-control"  id="icID" name="custIC" value="<?php echo $custIC;?>"/>
                        </div>
                        <div class="form-group">
                            <label>Marital Status</label>
                            <select  class="form-control" name="maritalStatusN"  id="maritalID" value="<?php echo $maritalStatus;?>">
                                <option value="<?php echo $maritalStatus;?>"> <?php echo $maritalStatus;?></option>
                                <option value="SINGLE">SINGLE</option>
                                <option value="MARRIED">MARRIED</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Occupation</label>
                            <input type="text" class="form-control" id="occuID" name="occupation" value="<?php echo $occupation;?>"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Phone Number</label>
                            <input type="text" class="form-control"  id="phoneID" name="phoneNo" value="<?php echo $phoneNo;?>"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Email Address</label>
                            <input type="email" class="form-control"  id="emailID" name="email" value="<?php echo $email;?>" aria-describedby="emailHelp"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Address</label>
                            <input type="text" class="form-control"  id="addID" name="address" value="<?php echo $address;?>"/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Booking Date</label>
                            <input type="date" class="form-control"  id="bdateID" name="bookingDate1" value="<?php echo $bookDate;?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Booking Fee</label>
                            <input type="text" class="form-control" id="bfeeID"  name="bookingFee" value="<?php echo $bookingFee;?>"/>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Car Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="text-field">Model</label>
                            <select class="custom-select"name="model" id="modelid" class="modelclass"  onChange="getVariant(this.value);">
                                <option value="<?php echo $carmodel; ?>"><?php echo $model; ?></option>
                                    <?php
                                    $sql = "SELECT * FROM car";
                                    $res = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_assoc($res))
                                    {
                                        if ($bul[$row['model']] != true && $row['model'] != 'model')
                                        {
                                    ?>
                                            <option value="<?php echo $row['model']; ?>"><?php echo $row['model']; ?></option>
                                        <?php
                                            $bul[$row['model']] = true;
                                        }
                                    }
                                        ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Variant</label>
                            <select class="custom-select" name="variant" id="variantid" class="variantclass"  onChange=" getColor(this.value); ">
                                <option value="<?php echo $variant; ?>"><?php echo $variant; ?></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="text-field">Color</label>
                            <select class="custom-select" name="color" id="colorid" class="colorclass" >
                                <option value="<?php echo $carID; ?>"><?php echo $color; ?></option>
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Finance Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label for="text-field">Amount</label>
                                    <input type="text" class="form-control" name="loanAmountN" id="loanAmountID" value="<?php echo $loanAmount;?>" placeholder="Enter Amount" >
                                </div>
                                <div class="form-group"  id="hidden-field3" >
                                    <label>Loan Period</label>
                                    <input type="text" class="form-control" name="loanPeriodN"  id="loanPeriodID" value="<?php echo $loanPeriod;?>"/>
                                </div>
                                <div class="form-group" id="hidden-field" >
                                    <label for="text-field">Bank</label>
                                    <input type="text" class="form-control" name="bank" id="bankID" value="<?php echo $bank;?>" placeholder="Enter Bank" >
                                </div>
                                <div class="form-group"  id="hidden-field2" >
                                    <label>LOU Number</label>
                                    <input type="text" class="form-control" name="louNo"  id="louID" value="<?php echo $louNo;?>" placeholder="Enter LOU Number" >
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="text-field">Payment Type</label>
                                    <select class="custom-select" name='paymentType' id="loanTypeID" value="<?php echo $paymentType;?>"  placeholder="Choose Payment" >
                                        <option value="<?php echo $paymentType;?>"> <?php echo $paymentType;?></option>
                                        <option value="CASH">CASH</option>
                                        <option value="LOAN">LOAN</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div> <br><br><br> </div>
                                </div>

                                <div class="form-group"  id="hidden-field4" >
                                    <label>Bank Branch</label>
                                    <input type="text" class="form-control" name="branchN"  id="branchID" value="<?php echo $branch;?>"/>
                                </div>
                                <div class="form-group"  id="hidden-field5" >
                                    <label>LOU Date</label>
                                    <input type="date" class="form-control" name="louDate"  id="datelouID" value="<?php echo $louDate;?>"/>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->

            <!-- right column -->
            <div class="col-md-6">
                <!-- Form Element sizes -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Insurance Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company</label>
                                    <input type="text" class="form-control" name="insuranceCompanyN"  id="insuranceID" value="<?php echo $insuranceCompany;?>" placeholder="Enter Insurance" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="text-field">Policy Number</label>
                                    <input type="text" class="form-control" name="policyN"  id="policyID" value="<?php echo $policyNo;?>" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label for="text-field">From</label>
                                    <input type="date" class="form-control" name="periodfromN"  id="periodfromID" value="<?php echo $periodfrom;?>" placeholder="Enter Bank" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="text-field">To </label>
                                    <input type="date" class="form-control" name="periodtoN"  id="periodtoID" value="<?php echo $periodto;?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <!-- general form elements disabled -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Allocation Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Engine Number</label>
                                    <input type="text" class="form-control" name="engineNo" id="engineID" value="<?php echo $engineNo;?>" placeholder="Enter Engine Number" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Year Make</label>
                                    <input type="text" class="form-control" name="yearMake" id="yearID" value="<?php echo $yearMake;?>" placeholder="Enter Year Make" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Chasis Number</label>
                                    <input type="text" class="form-control" name="chasisNo" id="chasisID" value="<?php echo $chasisNo;?>" placeholder="Enter Chasis Number" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Allocate Date</label>
                                    <input type="date" class="form-control" name="allocateDate" id="alloID" value="<?php echo $allocateDate;?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Downpayment Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table">
                            <thead>
                                <tr>
                                    <th>Amount</th>
                                    <th>Payment Method</th>
                                    <th>Receipt No.</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php
                                    while ($res5 = mysqli_fetch_array($results5)) {
                                    ?>
                                        <td>
                                            <?php echo $res5['downAmount'];?>
                                        </td>
                                        <td>
                                            <?php echo$res5['downMethod'];?>
                                        </td>
                                        <td>
                                            <?php echo $res5['downReceipt'];?>
                                        </td>
                                </tr>
                                    <?php } ?>
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Registration Details</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Register Date</label>
                                        <input type="date" class="form-control"  name="registerDate" id="registID" value="<?php echo $registerDate;?>" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Plate Number</label>
                                        <input type="text" class="form-control" name="plateNo" id="plateID" value="<?php echo $plateNo;?>" placeholder="Enter Plate Number" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Road Tax Expired Date</label>
                                        <input type="date" class="form-control" name="roadTax" id="roadtaxID" value="<?php echo $roadTax;?>" placeholder="Enter Road Tax" >
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Disbursement Details</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Date return VSO & VDO</label>
                                    <input type="date" class="form-control" id="dreturnID" name="returnDate" value="<?php echo $returnDate;?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Bank Document Date</label>
                                    <input type="date" class="form-control"  id="dbankID" name="bankDate" value="<?php echo $bankDate;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Collection Date</label>
                                    <input type="date" class="form-control"  id="dcollID" name="collectionDate" value="<?php echo $collectionDate;?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Bank Disbursed Date</label>
                                    <input type="date" class="form-control"  id="dburstID" name="disbursedDate" value="<?php echo $disbursedDate;?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- general form elements disabled -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Booking Status</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <label>Book Status</label>
                            <select class="custom-select"  id="bookstatID" name="bookStatus"  value="<?php echo $bookStatus;?>" >
                            <option value="<?php echo $bookStatus;?>">
                            <?php
                            if ( $bookStatus == 0){ echo "REGISTERED*";}
                            elseif ( $bookStatus == 1){ echo "REGISTERED";}
                            elseif ( $bookStatus == 2){ echo "ALLOCATED";}
                            elseif ( $bookStatus == 3){ echo "DE-ALLOCATE";}
                            elseif ( $bookStatus == 4){ echo "NEW ORDER";}
                            elseif ( $bookStatus == 5){ echo "DELIVERED";}
                            else{ echo "CANCELED";}
                            ?>
                            </option>
                            <option value="0">REGISTERED*</option>
                            <option value="1">REGISTERED</option>
                            <option value="2">ALLOCATED</option>
                            <option value="3">NEW ORDER</option>
                            <option value="4">DELIVERED</option>
                            <option value="5">CANCEL</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Notes</label>
                            <textarea class="form-control" rows="2"  id="reasonID" name="reason"  placeholder=" reason or notes"><?php echo $reason;?></textarea>
                        </div>
                        <div class="form-group">
                            <label>SAS Notes</label>
                            <textarea class="form-control" rows="2"  id="reasonAdminID" name="reasonAdminN"  placeholder=" reason or notes"><?php echo $reasonAdmin;?></textarea>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Sale Advisor :</label>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="saN" value="<?php echo $sa;?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <a type="button" class="btn btn-primary btn-sm" href="genPdf.php?id=<?php echo $bookID; ?>" target="_blank"> View Pdf </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (right) -->

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a class="btn btn-primary" href="insuranceListManager.php">Back</a>
                    </div>
                </div>
            </div>

          </div>
          <!-- /.row -->
        </form>
        <form method="post" class="form-upload" action="" enctype="multipart/form-data">
          <div class="row">
            <div class="col-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Upload Document</h3>
                    </div>
                    <div class="card-body">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                <th style="width: 10px">No</th>
                                <th>File Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <?php
                                $result = mysqli_query($conn, "SELECT * FROM uploaddoc where bookID='$bookID'");
                                $number=1;
                                while($user_data=mysqli_fetch_array($result)){
                                    $id=$user_data['bookID'];
                                ?>
                                    <td><?php echo $number++;?></td>
                                    <td><a href="uploads/<?php echo $bookID.'/'.$user_data['file_name'] ?>" target="_blank"><?php echo $user_data['file_name'] ?></a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </form>

      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Version 2.0
    </div>
    <!-- Default to the left -->
    
  </footer>
</div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- Page specific script -->
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
<script>
function getColor(val) {
$.ajax({
    url: "get_color.php",
    data:'color_id='+val,
    success: function(data){
    $("#colorid").html(data);
}
});
}

function getVariant(val) {
$.ajax({
    type: "POST",
    url: "get_variant.php",
    data:'variant_id='+val,
    success: function(data){
    $("#variantid").html(data);
}
});
}
</script>
<script>
$("#requestID").click(function(event){
    <?php
    echo "return confirm('Are you sure you want to request jump queue?')";
    if (isset($_POST['requestjump'])) {
        $query5 = "UPDATE booking SET jump=1 WHERE bookID='$bookID'";
        mysqli_query($conn, $query5);
    }

    ?>

});

$("#approveID").click(function(event){
    <?php
    echo "return confirm('Are you sure you want to approve jump queue?')";
    if (isset($_POST['approvejump'])) {
        $query6 = "UPDATE booking SET jump=2, bookStatus=0 WHERE bookID='$bookID'";
        mysqli_query($conn, $query6);


    }
    ?>
});

$("#rejectID").click(function(event){
    <?php
    echo "return confirm('Are you sure you want to reject jump queue?')";
    if (isset($_POST['rejectjump'])) {
            $query7 = "UPDATE booking SET jump=3 WHERE bookID='$bookID'";
            mysqli_query($conn, $query7);
    }
    ?>
});

</script>
<script>

  $(document).ready(function () {
    if ($("#loanTypeID").val() ==  "LOAN" ) {
        document.getElementById('hidden-field').hidden = false;
        document.getElementById('hidden-field2').hidden = false;
        document.getElementById('hidden-field3').hidden = false;
        document.getElementById('hidden-field4').hidden = false;
        document.getElementById('hidden-field5').hidden = false;
      } else {
        document.getElementById('hidden-field').hidden = true;
        document.getElementById('hidden-field2').hidden = true;
        document.getElementById('hidden-field3').hidden = true;
        document.getElementById('hidden-field4').hidden = true;
        document.getElementById('hidden-field5').hidden = true;
      }
    $("#loanTypeID").change(function () {
      if ($("#loanTypeID").val() ==  "LOAN" ) {
        document.getElementById('hidden-field').hidden = false;
        document.getElementById('hidden-field2').hidden = false;
        document.getElementById('hidden-field3').hidden = false;
        document.getElementById('hidden-field4').hidden = false;
        document.getElementById('hidden-field5').hidden = false;
      } else {
        document.getElementById('hidden-field').hidden = true;
        document.getElementById('hidden-field2').hidden = true;
        document.getElementById('hidden-field3').hidden = true;
        document.getElementById('hidden-field4').hidden = true;
        document.getElementById('hidden-field5').hidden = true;
      }
    })
  });

</script>
<script>
        document.getElementById('custID').readOnly = true;
        document.getElementById('icID').readOnly = true;
        document.getElementById('maritalID').disabled = true;
        document.getElementById('occuID').readOnly = true;
        document.getElementById('phoneID').readOnly = true;
        document.getElementById('emailID').readOnly = true;
        document.getElementById('addID').readOnly = true;
        document.getElementById('bfeeID').readOnly = true;
        document.getElementById('receiptID').readOnly = true;

        document.getElementById('modelid').disabled = true;
        document.getElementById('variantid').disabled = true;
        document.getElementById('colorid').disabled = true;

        document.getElementById('vsoID').readOnly = true;

        document.getElementById('louID').readOnly = true;
        document.getElementById('bankID').readOnly = true;
        document.getElementById('loanPeriodID').readOnly = true;
        document.getElementById('branchID').readOnly = true;
        document.getElementById('datelouID').disabled = true;
        document.getElementById('loanAmountID').readOnly = true;
        document.getElementById('loanTypeID').disabled = true;

        document.getElementById('insuranceID').readOnly = true;
        document.getElementById('policyID').readOnly = true;
        document.getElementById('periodfromID').readOnly = true;
        document.getElementById('periodtoID').readOnly = true;

        document.getElementById('engineID').readOnly = true;
        document.getElementById('chasisID').readOnly = true;
        document.getElementById('yearID').readOnly = true;
        document.getElementById('alloID').readOnly = true;



        document.getElementById('registID').readOnly = true;
        document.getElementById('plateID').readOnly = true;
        document.getElementById('roadtaxID').readOnly = true;

        document.getElementById('dreturnID').readOnly = true;
        document.getElementById('dbankID').readOnly = true;
        document.getElementById('dcollID').readOnly = true;
        document.getElementById('dburstID').readOnly = true;

        document.getElementById('bookstatID').disabled = true;
        document.getElementById('reasonID').readOnly = true;
        document.getElementById('reasonAdminID').readOnly = true;

        document.getElementById('updateBtnID').disabled = true;


</script>
</body>
</html>
